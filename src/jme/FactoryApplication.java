package jme;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.AssetManager;
import com.jme3.cinematic.Cinematic;
import com.jme3.cinematic.MotionPath;
import com.jme3.cinematic.MotionPathListener;
import com.jme3.cinematic.events.MotionEvent;
import com.jme3.light.DirectionalLight;
import com.jme3.math.Spline.SplineType;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.system.Timer;

public class FactoryApplication extends SimpleApplication {

	final static String RED_BOX_MODEL = "Models/KisteROT/KisteROT.j3o";
	final static String GREEN_BOX_MODEL = "Models/KisteGRUEN/KisteGRUEN.j3o";
	final static float BOX_MODEL_SCALE = 0.75f;
	final static float SPACE_BETWEEN_BOXES = 0.75f;
	final static Vector3f FIRST_BOX_POSITION = new Vector3f(-0.25f, -1.25f, 2.25f);

	final static String PRODUCTION_LINE_MODEL = "Models/laufband/laufband.j3o";
	final static Vector3f PRODUCTION_LINE_LOCATION = new Vector3f(0.0f, -1.5f, 0.0f);
	final static float PRODUCTION_LINE_SCALE = 1.0f;

	final static Vector3f SPLIT_WAY_POINT = new Vector3f(-0.25f, -1.3f, 0);
	final static float MAXIMAL_DISTANCE_AFTER_SPLIT = 4.0f;

	private Spatial[] boxes = new Spatial[10];
	private boolean[] bricks;
	private boolean[] scrapeOrder;
	private boolean playAnimation; 

	private MotionEvent[] motionEvents = new MotionEvent[10];

	public FactoryApplication(boolean bricks[], boolean[] scrapeOrder, boolean playAnimation) {
		this.bricks = bricks;
		this.scrapeOrder = scrapeOrder; 
		this.playAnimation = playAnimation; 
	}


	private void placeStaticModels(Node rootNode, AssetManager assetManager) {
		Spatial laufband = assetManager.loadModel(PRODUCTION_LINE_MODEL);
		laufband.scale(PRODUCTION_LINE_SCALE);
		laufband.setLocalTranslation(PRODUCTION_LINE_LOCATION);
		rootNode.attachChild(laufband);
	}

	private void placeDynamicModels(Node rootNode, AssetManager assetManager, boolean[] bricks) {

		for (int i = 0; i < bricks.length; i++) {
			String modelpath = (bricks[i]) ? GREEN_BOX_MODEL : RED_BOX_MODEL;
			Spatial box = assetManager.loadModel(modelpath);
			box.scale(BOX_MODEL_SCALE);

			box.setLocalTranslation(FIRST_BOX_POSITION.add(0.0f, 0.0f, SPACE_BETWEEN_BOXES * i));
			boxes[i] = box;
			rootNode.attachChild(boxes[i]);
		}

	}

	private void setMotionPaths(Node rootNode, AssetManager assetManager, boolean[] scrapeOrder) {
		int redBoxesProcessed = 0;
		int greenBoxesProcessed = 0;

		for (int i = 0; i < boxes.length; i++) {
			MotionPath path = new MotionPath();
			path.setPathSplineType(SplineType.Linear);
			path.setCurveTension(0.0f);

			for (int j = i; j >= 0; j--) {
				path.addWayPoint(boxes[j].getWorldTranslation());
			}

			path.addWayPoint(SPLIT_WAY_POINT);
			if (scrapeOrder[i]) {
				path.addWayPoint(SPLIT_WAY_POINT.add(0, 0,
						-(MAXIMAL_DISTANCE_AFTER_SPLIT - greenBoxesProcessed * SPACE_BETWEEN_BOXES)));
				greenBoxesProcessed++;
			} else {
				path.addWayPoint(SPLIT_WAY_POINT
						.add(MAXIMAL_DISTANCE_AFTER_SPLIT - redBoxesProcessed * SPACE_BETWEEN_BOXES, 0, 0));
				redBoxesProcessed++;
			}

			motionEvents[i] = new MotionEvent(boxes[i], path);

		}
	}

	public void playAnimation() {
		Cinematic cinematic = new Cinematic(this.getRootNode(), 200.0f);
		for (int i = 0; i < motionEvents.length; i++) {
			cinematic.addCinematicEvent(0.00f + i * 3.0f, motionEvents[i]);

		}
		this.getStateManager().attach(cinematic);
		cinematic.play();

	}

	@Override
	public void simpleInitApp() {

		placeStaticModels(rootNode, assetManager);
		placeDynamicModels(rootNode, assetManager, bricks);
		setMotionPaths(rootNode, assetManager, scrapeOrder);
		if(playAnimation) {
			playAnimation(); 
		}
		
		// lighting
		DirectionalLight sun = new DirectionalLight();
		sun.setDirection(new Vector3f(-0.1f, -0.7f, -1.0f));
		rootNode.addLight(sun);

		// camera settings
		flyCam.setMoveSpeed(5.0f);
		//flyCam.setEnabled(false);

	}

}
