package factory.template;

import factory.mission1.Brick;
import factory.mission1.Mission1;
 

public class Mission1Template {
	
	static Mission1 mission1 = new Mission1(); 

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		boolean[] scrapeOrder = {false, true, false, true, false, true, false, true, false, true};
		initFactory(getScrapeOrder()); 
	}
	
	// Edit this method
	private static boolean[] getScrapeOrder() {
		boolean[] bricks = new boolean[10]; 
		for(int i = 0; i < getBricks().length; i++) {
			bricks[i] = (getBricks()[i].getQuality() < 6) ? false : true; 
		}
		return bricks; 
	}
	
	/*
	 * ---------------------------------
	 * DO NOT EDIT CODE PAST THIS POINT!
	 * ---------------------------------
	 * 
	 * 
	 * 
	 * 
	 * 
	 */

	// Helper Methods 
	private static void initFactory() {
		 
		mission1.initFactory();
	}
	
	private static void initFactory(boolean[] scrapeOrder) {
		
		mission1.initFactory(scrapeOrder);
	}
	
	private static Brick[] getBricks() {
		return mission1.getBricks(); 
	}
	
	

}
