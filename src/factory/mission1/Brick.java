package factory.mission1;

public class Brick {
	int quality;
	
	public Brick(int quality) {
		this.quality = quality; 
	}
	
	public int getQuality() {
		return quality; 
	}
	
	
}