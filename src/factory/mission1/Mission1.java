package factory.mission1;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import jme.FactoryApplication;

public class Mission1 {
	
	FactoryApplication fa; 
	
	private Brick[] bricks = new Brick[10]; 
	private static final int FAULTY_MIN = 2;
	private static final int FAULTY_MAX = 4; 

	public Mission1() {
		int faulty = ThreadLocalRandom.current().nextInt(FAULTY_MIN, FAULTY_MAX + 1);
		for(int i = 0; i < faulty; i++) {
			int quality = ThreadLocalRandom.current().nextInt(1, 5 + 1);
			bricks[i] = new Brick(quality);
		}
		for(int i = faulty; i< 10; i++) {
			int quality = ThreadLocalRandom.current().nextInt(6, 10 + 1);
			bricks[i] = new Brick(quality); 
		}
		shuffleArray(bricks); 
		
		
		
		
	}
	
	// Get Bricks
	public Brick[] getBricks() {
		return bricks; 
	}
	
	private boolean[] getBricksAsBoolean() {
		boolean[] bricks = new boolean[10]; 
		for(int i = 0; i < getBricks().length; i++) {
			bricks[i] = (getBricks()[i].getQuality() < 6) ? false : true; 
		}
		return bricks; 
	}
	
	public void initFactory() {
		fa = new FactoryApplication(getBricksAsBoolean(), getBricksAsBoolean(), false); 
		fa.start(); 
		
	}
	
	public void initFactory(boolean[] scrapeOrder) {
		fa = new FactoryApplication(getBricksAsBoolean(), scrapeOrder, true); 
		fa.start();
		
	}
	

	
	//Fisher-Yates shuffle
	static void shuffleArray(Brick[] ar){
	    // If running on Java 6 or older, use `new Random()` on RHS here
	    Random rnd = ThreadLocalRandom.current();
	    for (int i = ar.length - 1; i > 0; i--)
	    {
	      int index = rnd.nextInt(i + 1);
	      // Simple swap
	      Brick a = ar[index];
	      ar[index] = ar[i];
	      ar[i] = a;
	    }
	  }
	
	
}
